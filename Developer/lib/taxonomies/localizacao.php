<?php

    function adiciona_localizacao(){

        $labels = array(
            'name'            => 'Localizações',
            'singular_name'   => 'localização',
            'edit_item'       => 'editar itens',
            'add_new_item'    => 'adicionar item'
        );

        $args = array(
            'labels'          => $labels,
            'public'          => true,
            'hierarchical'    => true
        );

        register_taxonomy('localizacao', 'imovel', $args);
    }
    
    add_action('init', 'adiciona_localizacao');