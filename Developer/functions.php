<?php 

    add_theme_support('post-thumbnails');

    add_action('init', 'bootstrap');

    function bootstrap(){
        wp_enqueue_style('bootstrap',"https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css");
    }

    require_once __DIR__ . '/lib/meta/meta-casa.php';

    function menu_principal(){
       register_nav_menu('header-menu','main-menu');
    }

    add_action('init', 'menu_principal');
    
    function seletor_title(){
        bloginfo();
        if(!is_home()) { echo ' | ';};
        the_title();
    }

    require_once __DIR__ . '/lib/taxonomies/localizacao.php';