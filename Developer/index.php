<?php
    $taxonomy_especifico = array_key_exists('taxonomy', $_GET);
    if($taxonomy_especifico && $_GET['taxonomy'] === ''){
        wp_redirect(home_url());
    };
?>
    <h1 class="jumbotron h1 text-center">Developer Theme</h1></h1>
    <?php $taxonomies = get_terms('localizacao')?>
    <form action="<?= bloginfo('url') ?>" method="GET">
            <div class="input-group">
                <select class="custom-select" name="taxonomy">
                    <option value="">Todos</option>
                    <?php foreach($taxonomies as $taxonomy) : ?>
                         <option value="<?= $taxonomy->slug ?>"><?= $taxonomy->name ?></option>
                    <?php endforeach; ?>
                </select>
                <div class="input-group-append">
                    <button class="btn btn-outline-secondary" type="submit">Search</button>
                </div>
           </div>
    </form>
    <?php
    if($taxonomy_especifico){
        $taxquery = array(
            array(
                'taxonomy'     => 'localizacao',
                'field'        => 'slug',
                'terms'        => $_GET['taxonomy']
            )
        );
    };
        $args = array(
            'post_type'        => 'imovel',
            'tax_query'        => $taxquery
        );
        $loop = new WP_Query($args);
    ?>
    <div class="container">
        <div class="row">
            <?php if($loop->have_posts()) : while($loop->have_posts()) : $loop->the_post()?>
                <div class="col-md-4">
                    <a href="<?php the_permalink()?>">
                        <div class="card" style="width: 18rem;">
                            <img class="card-img-top img-fluid" <?= the_post_thumbnail();?>
                            <div class="card-body">
                                <h5 class="card-title text-center"><?php the_title() ?></h5>
                                <p class="card-text"><?php the_content() ?></p>
                            </div>
                        </div>
                    </a>
                </div>
            <?php endwhile; endif; ?>
        </div>
    </div>
<?php get_footer() ?>
