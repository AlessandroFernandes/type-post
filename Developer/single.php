<?php get_header() ?>

    <?php  if(have_posts()) : while(have_posts()) : the_post()?>

        <?php
             the_post_thumbnail();
             the_title();
             the_content();
             the_date();
        ?>
    <?php endwhile; endif;?>
<?php get_footer() ?>