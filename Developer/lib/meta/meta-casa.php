<?php
        function post_imoveis(){
        $nome       = "Imóveis";
        $nomeplural = "Imóvel";
        $descricao  = "Sistema de post para imóveis";

        $labels = array(
                'name'               => $nome,
                'singular_name'      => $nomeplural,
                'add_new_item'       => 'Adicionar imóvel',
                'edit_item'          => 'Editar imóvel',
                'new_item'           => 'Novo Imóvel',
                'search_items'       => 'Buscar Imóvel'
        );

        $supports = array(
                'title',
                'editor',
                'thumbnail'
        );

        $args = array(
                'labels'             => $labels,
                'public'             => true,
                'description'        => $descricao,
                'menu_icon'          => 'dashicons-admin-home',
                'supports'           => $supports
        );
        register_post_type('imovel', $args);
        };

        add_action('init', 'post_imoveis');