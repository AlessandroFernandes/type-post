<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">

    <?php $home = get_template_directory_uri() ?>
    <link rel="stylesheet" href="<?= $home ?>/assets/css/reset.css">
    <!-- <link rel="stylesheet" href="<?= $css_especifico ?>.css" > -->
    <title>
        <?php seletor_title() ?>
    </title>

    <?php wp_head() ?>

</head>
<body>
    
    <header>
        <?php  wp_nav_menu($args)?>
    </header>